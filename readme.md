# Magnon spin simulation

This project is one of three in the subject "TFY4235 - Computational Physics" at NTNU. The subject concerns numerical methods in physics and is based on three projects with a one week final numerical exam.

This project implements a numerical algorithm for the Landau-Lifshitz-Gilbert equation. A project task description is provided in `CompPhys_MagnonTask.pdf`. The system describes a simplified magnetic spin systems with an anisotropy direction (often chosen along the $z$-axis). Temperature is implemented to explore the statistical behavior of the system. 

The implementation is in Julia, where the `project2.ipynb` jupyter notebook can be ran with a Julia kernel after installing appropriate packages. The `spin3d.jl` file isolates the 3D spin lattice simulation from the rest of the code, is allocation free and quite optimized.

Some animations of the results from 1D are provided below. See `img` for more plots.

### Antisymmetric 1D spin chain
![antiferrous](./img/manispins_1D_z_minusJ.gif)

### Symmetric 1D spin chain
![ferrous](./img/manispins_1D_z_plusJ.gif)

### 1D spin wave with low damping
![wave](./img/spins100_1D_one_perturbed_wave_alpha00.gif)

### 1D spin wave with medium damping
![wave](./img/spins100_1D_one_perturbed_wave_alpha01.gif)