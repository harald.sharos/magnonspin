using LinearAlgebra
using Base.Threads
using StatsBase
using StaticArrays
using SIMD
using DelimitedFiles
using ProgressMeter

@threads for i in 1:Threads.nthreads()
    @show Threads.threadid()
end

"""
Physical parameters, constant throughout the simulation
Constructor SpinParams(B,B0,J,dz,α,dt,kbT,γ,μ)

# Fields
B::Vector{SVector{3, Float64}} ## Normalized magnetic field
B0::Float64 ## [A/m] Magnetic field strength
J::Float64 ## [eV] Exchange energy
μ::Float64 ## [eV/T] Magnetic moment
dz::Float64 ## [eV] Uniaxial anisotropy energy
J_div_μ::Float64 ## Convenience variable J/μ
dz_div_μ::Float64 ## Convenience variable dz/μ
α::Float64 ## Gilbert damping
K::Float64 ## [Hz/T] Prefactor in LLG equation (γ/(1+α^2))
β::Float64 ## [T] Prefactor in effective Hamiltonian (sqrt(2*α*kbT/(μ*γ*dt)))
dt::Float64 ## [s] Time step
"""
struct SpinParams3D

    B::Array{SVector{3,Float64},3} ## Normalized magnetic field
    B0::Float64 ## [A/m] Magnetic field strength
    J::Float64 ## [eV] Exchange energy
    μ::Float64 ## [eV/T] Magnetic moment
    dz::Float64 ## [eV] Uniaxial anisotropy energy
    J_div_μ::Float64 ## Convenience variable J/μ
    dz_div_μ::Float64 ## Convenience variable dz/μ
    α::Float64 ## Gilbert damping
    K::Float64 ## [Hz/T] Prefactor in LLG equation (γ/(1+α^2))
    β::Float64 ## [T] Prefactor in effective Hamiltonian (sqrt(2*α*kbT/(μ*γ*dt)))
    dt::Float64 ## [s] Time step

    function SpinParams3D(B::Array{SVector{3,Float64},3}, B0::Float64=3 / 5.8e-5, J::Float64=10e-3, dz::Float64=3e-3, α::Float64=0.0, dt::Float64=1e-15, kbT::Float64=0.0,
        γ::Float64=1.6e11, μ::Float64=5.8e-5)
        new(B, B0, J, μ, dz, J / μ, dz / μ, α, -γ / (1 + α^2), sqrt(2 * α * kbT / (μ * γ * dt)), dt)
    end

    function SpinParams3D(S::Array{SVector{3,Float64},3})
        SpinParams3D(fill(SVector(0.0, 0.0, 1.0), size(S)), 3 / 5.8e-5, 10e-3, 3e-3, 0.0, 1e-15, 0.0, 1.6e11, 5.8e-5)
    end

    function SpinParams3D(; B::Array{SVector{3,Float64},3}, B0::Float64=3 / 5.8e-5, J::Float64=10e-3, dz::Float64=3e-3, α::Float64=0.0, dt::Float64=1e-15, kbT::Float64=0.0,
        γ::Float64=1.6e11, μ::Float64=5.8e-5)
        SpinParams3D(B, B0, J, μ, dz, α, dt, kbT, γ, μ)
    end
end


"""
Preallocated data for the differential equation
Ensures only stack allocation of memory, for performance
Constructor PreAllocDiffData(S::Vector{SVector{3, Float64}})

# Fields
Heff::Vector{SVector{3, Float64}} -> Effective field
ξ::Vector{SVector{3, Float64}} -> Noise vector
interaction::Vector{SVector{3, Float64}} -> Exchange interaction
anisotropy::Vector{SVector{3, Float64}} -> Uniaxial anisotropy

dSp::Vector{SVector{3, Float64}} -> Predictor function eval
dSc::Vector{SVector{3, Float64}} -> Corrector function eval

Sp::Vector{SVector{3, Float64}} ## Normalized predictor step
Sc::Vector{SVector{3, Float64}} ## Normalized corrector step
"""
mutable struct PreAllocDiffData3D
    ## Preallocation of StaticArrays, for performance
    Heff::Array{SVector{3,Float64},3}
    ξ::Array{SVector{3,Float64},3}
    interaction::Array{SVector{3,Float64},3}
    anisotropy::Array{SVector{3,Float64},3}

    dSp::Array{SVector{3,Float64},3}
    dSc::Array{SVector{3,Float64},3}

    Sp::Array{SVector{3,Float64},3} ## Normalized predictor step
    Sc::Array{SVector{3,Float64},3} ## Normalized corrector step

    PreAllocDiffData3D(S::Array{SVector{3,Float64},3}) = new(
        similar(S), similar(S), similar(S), similar(S),
        similar(S), similar(S),
        similar(S), similar(S)
    )
end


function dS3D(dS::Array{SVector{3,Float64},3}, S::Array{SVector{3,Float64},3},
    p::SpinParams3D, prealloc::PreAllocDiffData3D)::Array{SVector{3,Float64},3}

    α = p.α
    K = p.K
    β = p.β
    J_div_μ = p.J_div_μ
    dz_div_μ = p.dz_div_μ
    B0 = p.B0
    B = p.B

    Ns = size(S, 1)

    Heff = prealloc.Heff
    ξ = prealloc.ξ
    interaction = prealloc.interaction
    anisotropy = prealloc.anisotropy

    # Calculate interaction term, with BCs

    ## Corners boundary conditions
    ## BC(x±1) + BC(y±1) + BC(z±1)
    ## for x,y,z = (1,end)
    interaction[1, 1, 1] = J_div_μ * (
        S[2, 1, 1] + S[1, 2, 1] + S[1, 1, 2] +
        S[end, 1, 1] + S[1, end, 1] + S[1, 1, end])

    interaction[1, 1, end] = J_div_μ * (
        S[2, 1, end] + S[1, 2, end] + S[1, 1, 1] +
        S[end, 1, end] + S[1, end, end] + S[1, 1, end-1])

    interaction[1, end, 1] = J_div_μ * (
        S[2, end, 1] + S[1, 1, 1] + S[1, end, 2] +
        S[end, end, 1] + S[1, end-1, 1] + S[1, end, end])

    interaction[1, end, end] = J_div_μ * (
        S[2, end, end] + S[1, 1, end] + S[1, end, 1] +
        S[end, end, end] + S[1, end-1, end] + S[1, end, end-1])

    interaction[end, 1, 1] = J_div_μ * (
        S[1, 1, 1] + S[end, 2, 1] + S[end, 1, 2] +
        S[end-1, 1, 1] + S[end, end, 1] + S[end, 1, end])

    interaction[end, 1, end] = J_div_μ * (
        S[1, 1, end] + S[end, 2, end] + S[end, 1, 1] +
        S[end-1, 1, end] + S[end, end, end] + S[end, 1, end-1])

    interaction[end, end, 1] = J_div_μ * (
        S[1, end, 1] + S[end, 1, 1] + S[end, end, 2] +
        S[end-1, end, 1] + S[end, end-1, 1] + S[end, end, end])

    interaction[end, end, end] = J_div_μ * (
        S[1, end, end] + S[end, 1, end] + S[end, end, 1] +
        S[end-1, end, end] + S[end, end-1, end] + S[end, end, end-1])

    ## Edges boundary conditions
    @inbounds @simd for i in 2:Ns-1
        #   Along x
        # Edge 1
        interaction[i, 1, 1] = J_div_μ * (
            S[i-1, 1, 1] + S[i, 2, 1] + S[i, 1, 2] +
            S[i+1, 1, 1] + S[i, end, 1] + S[i, 1, end])

        # Edge 2
        interaction[i, 1, end] = J_div_μ * (
            S[i-1, 1, end] + S[i, 2, end] + S[i, 1, 1] +
            S[i+1, 1, end] + S[i, end, end] + S[i, 1, end-1])

        # Edge 3
        interaction[i, end, 1] = J_div_μ * (
            S[i-1, end, 1] + S[i, 1, 1] + S[i, end, 2] +
            S[i+1, end, 1] + S[i, end-1, 1] + S[i, end, end])

        #Edge 4
        interaction[i, end, end] = J_div_μ * (
            S[i-1, end, end] + S[i, 1, end] + S[i, end, 1] +
            S[i+1, end, end] + S[i, end-1, end] + S[i, end, end-1])
    end

    @inbounds @simd for i in 2:Ns-1
        # Along y
        interaction[1, i, 1] = J_div_μ * (
            S[2, i, 1] + S[1, i-1, 1] + S[1, i, 2] +
            S[end, i, 1] + S[1, i+1, 1] + S[1, i, end])

        interaction[1, i, end] = J_div_μ * (
            S[2, i, end] + S[1, i-1, end] + S[1, i, 1] +
            S[end, i, end] + S[1, i+1, end] + S[1, i, end-1])

        interaction[end, i, 1] = J_div_μ * (
            S[1, i, 1] + S[end, i-1, 1] + S[end, i, 2] +
            S[end-1, i, 1] + S[end, i+1, 1] + S[end, i, end])

        interaction[end, i, end] = J_div_μ * (
            S[1, i, end] + S[end, i-1, end] + S[end, i, 1] +
            S[end-1, i, end] + S[end, i+1, end] + S[end, i, end-1])
    end

    @inbounds @simd for i in 2:Ns-1
        # Along z
        interaction[1, 1, i] = J_div_μ * (
            S[2, 1, i] + S[1, 2, i] + S[1, 1, i-1] +
            S[end, 1, i] + S[1, end, i] + S[1, 1, i+1])

        interaction[1, end, i] = J_div_μ * (
            S[2, end, i] + S[1, 1, i] + S[1, end, i-1] +
            S[end, end, i] + S[1, end-1, i] + S[1, end, i+1])

        interaction[end, 1, i] = J_div_μ * (
            S[1, 1, i] + S[end, 2, i] + S[end, 1, i-1] +
            S[end-1, 1, i] + S[end, end, i] + S[end, 1, i+1])

        interaction[end, end, i] = J_div_μ * (
            S[1, end, i] + S[end, 1, i] + S[end, end, i-1] +
            S[end-1, end, i] + S[end, end-1, i] + S[end, end, i+1])
    end

    ## Interior interactions
    @inbounds @simd for i in 2:Ns-1
        for j in 2:Ns-1
            for k in 2:Ns-1
                interaction[i, j, k] = J_div_μ * (
                    S[i-1, j, k] + S[i+1, j, k] +
                    S[i, j-1, k] + S[i, j+1, k] +
                    S[i, j, k-1] + S[i, j, k+1])
            end
        end
    end

    ## Calculate anisotropy term
    @inbounds @simd for i in eachindex(S)
        anisotropy[i] = 2 * dz_div_μ * SVector{3,Float64}(0.0, 0.0, S[i][3])
    end

    ## Calculate effective field
    @inbounds @simd for i in eachindex(S)
        Heff[i] = interaction[i] + anisotropy[i] + B0 * B[i] + ξ[i]
    end

    ## Differential equation for the spin vector
    @inbounds @simd for i in eachindex(S)
        # Differential equation for the spin vector
        dS[i] = K * S[i] × (Heff[i] + α * (S[i] × Heff[i]))
    end

    return dS
end


function heun_step_3D(S::Array{SVector{3,Float64},3}, p::SpinParams3D, prealloc::PreAllocDiffData3D)::Array{SVector{3,Float64},3}

    dt = p.dt
    N = length(S)
    dSc = prealloc.dSc
    dSp = prealloc.dSp
    Sp = prealloc.Sp
    Sc = prealloc.Sc
    ξ = prealloc.ξ
    β = p.β

    ## Make some noise!
    @inbounds @simd for i in eachindex(S)
        ξ[i] = β * SVector{3,Float64}(randn(), randn(), randn())
    end


    dSc = dS3D(dSc, S, p, prealloc) ## f(tn, S) - corrector func eval

    # Predictor part
    @inbounds @simd for i in eachindex(S)
        ## S^p_{n+1} - predictor step
        Sp[i] = (S[i] + dSc[i] * dt) / norm(S[i] + dSc[i] * dt)
    end

    dSp = dS3D(dSp, Sp, p, prealloc) ## f(tn, Sp) - predictor func eval

    # Corrector part

    @inbounds @simd for i in eachindex(S)
        ## S^c_{n+1} - corrector step
        S[i] = (S[i] + (dSc[i] + dSp[i]) * 0.5 * dt) / norm(S[i] + (dSc[i] + dSp[i]) * 0.5 * dt)
    end

    return S
end


function magnetization(S::Array{SVector{3,Float64},3})
    cum = 0.0
    @inbounds @simd for i in eachindex(S)
        cum += S[i][3]
    end
    return cum / length(S)
end


function sim_ground_state_3d(; case=1, kbT_factor=0.0)::Tuple{Vector{Float64},SpinParams3D}
    Ns = 32

    J = 10e-3
    B = fill(SVector(0.0, 0.0, 1.0), (Ns, Ns, Ns))
    # B0 = 3/5.8e-5
    B0 = 0.0
    dz = J
    α = 0.01
    dt = 0.5e-15
    kbT = kbT_factor * J
    γ = 1.6e11
    μ = 5.8e-5
    S = Array{SVector{3,Float64},3}(undef, Ns, Ns, Ns)
    if case == 1
        S = fill(SVector(rand() * 2.0 - 1.0, rand() * 2.0 - 1.0, rand() * 2.0 - 1.0), (Ns, Ns, Ns))
    elseif case == 2
        S = fill(SVector(0.0, 0.0, 1.0), (Ns, Ns, Ns))
        kbT = 0.1J
    elseif case == 3
        S = fill(SVector(0.0, 0.0, 1.0), (Ns, Ns, Ns))
        kbT = kbT_factor * J
    end

    dS = similar(S)
    prealloc = PreAllocDiffData3D(S)

    for i in eachindex(S)
        S[i] = S[i] / norm(S[i])
    end



    p = SpinParams3D(B, B0, J, dz, α, dt, kbT, γ, μ)

    # tf = 5e-13 # debug
    tf = 1e-11
    Nt = round(Int, tf / dt)

    Mt = Vector{Float64}(undef, Nt + 1)
    Mt[1] = magnetization(S)

    for i in 2:Nt+1
        S = heun_step_3D(S, p, prealloc)
        Mt[i] = magnetization(S)
    end

    return Mt, p
end

const DATADIR = "data/"
function main()
    
    Nk = 6*20 # 6 threads on home PC
    kmin = 1.0
    kmax = 2.0
    kbT_factors = collect(range(kmin, kmax, length=Nk))
    progress = Progress(Nk, 1, "Simulating ground states...")
    @threads for i in eachindex(kbT_factors)
        Mt, p = sim_ground_state_3d(case=3, kbT_factor=kbT_factors[i])
        writedlm(DATADIR*"mag_ground_state_$(kmin)-$(kmax)_N$(Nk)_$(i).csv", Mt, ',')
        next!(progress)
    end
    finish!(progress)

    return nothing
end


main()