using CairoMakie, DelimitedFiles, StatsBase, LaTeXStrings
CairoMakie.activate!(type="pdf")
Makie.inline!(false)

ax_theme = (
    backgroundcolor=:white,
    leftspinevisible=true,
    rightspinevisible=true,
    bottomspinevisible=true,
    topspinevisible=true,
    bottomspinecolor=RGBAf(0, 0, 0, 0.20),
    leftspinecolor=RGBAf(0, 0, 0, 0.20),
    rightspinecolor=RGBAf(0, 0, 0, 0.20),
    topspinecolor=RGBAf(0, 0, 0, 0.20),
    xgridcolor=RGBAf(0, 0, 0, 0.20),
    ygridcolor=RGBAf(0, 0, 0, 0.20),
    xticksize=10,
    yticksize=10,
    ylabelrotation=0,
    xlabelpadding=10,
    ylabelpadding=20,
)
theme = Theme(fonts=(; regular="Arial", bold="Arial Bold"),
    gridcolor=:grey,
    gridalpha=0.5,
    fontsize=32,
    linewidth=2,
    markersize=8,
    resolution=(1200, 800),
    Axis=ax_theme,
)
set_theme!(theme)
Makie.theme(:fonts)

# Load data
Ms = Vector{Matrix{Float64}}(undef, 120)

for i in eachindex(Ms)
    Ms[i] = readdlm("data/mag_ground_state_1.0-2.0_N120_$(i).csv")
end
size(Ms)
Nkbt = length(Ms)
kbts = range(1.0, 2.0, length=Nkbt)
N = length(Ms[1])
Nhalf = round(Int, N / 2)
Tavgs = [mean(M[Nhalf:end]) for M in Ms]
@show Tavgs
idx = Tavgs .== NaN
i = 1
while i < length(Tavgs)
    if isnan(Tavgs[i])
        break
    end
    i += 1
end
display(i)
# Plot
begin
    fig = Figure(resolution=(1200, 800))
    ax = Axis(fig[1, 1], xlabel=L"$k_bT/J$", ylabel=L"$$M(T)")
    lines!(ax, kbts, Tavgs, color=:black, linewidth=2)

    save("task2_1.0-2.0.pdf", fig)
end

cscs = findall(x -> isapprox(x, 1.81, atol=0.05), kbts)
Mc = Ms[cscs]
Mc = reduce(hcat, Ms)
Mc |> sum |> sum
findall.(Mc)